﻿using UnityEngine;
using System.Collections;

public class StateMachineTest : StateMachine
{
	// Use this for initialization
	void Start () 
    {
        IEnumeratorFactory DEFAULT_ENTER_START = Default_Enter_Start;
        IEnumeratorFactory DEFAULT_ENTER_END = Default_Enter_End;

	    defaultOnEnter.Add( "end" , new DefaultTransition( DEFAULT_ENTER_START , true , LockWait.forBoth , ChangeState.afterBoth ) );
	}
	
	
    private IEnumerator Default_Enter_Start ()
    {
        yield return null;
    }

    private IEnumerator Default_Enter_End()
    {
        yield return null;
    }
}
