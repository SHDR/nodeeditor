﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class StateMachine : MonoBehaviour
{

#region Initialization
    
	private void Awake()
	{
        if ( states.Length > 0 )
        {
            foreach ( string state in states )
                _states.Add( state );

            if ( !string.IsNullOrEmpty( initialState ) && _states.Contains( initialState ) )
                SetState( initialState );
            else
                SetState( states[ 0 ] );
        }
        else
            Debug.Log( "No states found!" );
	}

#endregion

#region State

	public void SetState( string newState )
	{
		if ( gameObject.activeInHierarchy && _states.Contains( newState ) && !_locked && newState != currentState )
		{
            StatePair statePair = new StatePair( currentState , newState );

			if ( customTransition.ContainsKey( statePair ) )
			{
				CustomTransition transition = customTransition[ statePair ];

				if ( transition.locked )
				{
					_locked = true;
					_lockWait = transition.waitFor;
				}

				_upcomingState = newState;
				
				StartCoroutine( TransitionStepper( transition.enterTransition ) );
				StartCoroutine( TransitionStepper( transition.exitTransition ) );
			}
			else if ( !string.IsNullOrEmpty( statePair.exitingState ) || !string.IsNullOrEmpty( statePair.enteringState ) )
			{
                if ( !string.IsNullOrEmpty( statePair.exitingState ) && defaultOnExit.ContainsKey( statePair.exitingState ) )
				{
                    DefaultTransition transition = defaultOnExit[ statePair.exitingState ];

                    if ( string.IsNullOrEmpty( statePair.enteringState ) || !defaultOnEnter.ContainsKey( statePair.enteringState ) )
                    {
                        if ( transition.waitFor == LockWait.forBoth  )
                            transition.waitFor = LockWait.forEither;
                        
                        if ( transition.changeState == ChangeState.afterBoth )
                            transition.changeState = ChangeState.afterEither;
                    }

					if ( transition.locked )
					{
						_locked = true;
						_lockWait = transition.waitFor;
					}

					_upcomingState = newState;
					
					StartCoroutine( TransitionStepper( transition.transition ) );
				}

                if ( !string.IsNullOrEmpty( statePair.enteringState ) && defaultOnEnter.ContainsKey( statePair.enteringState ) )
				{
                    DefaultTransition transition = defaultOnEnter[ statePair.enteringState ];

                    if ( string.IsNullOrEmpty( statePair.exitingState ) || !defaultOnExit.ContainsKey( statePair.exitingState ) )
                    {
                        if ( transition.waitFor == LockWait.forBoth )
                            transition.waitFor = LockWait.forEither;

                        if ( transition.changeState == ChangeState.afterBoth )
                            transition.changeState = ChangeState.afterEither;
                    }

					if ( transition.locked )
					{
						_locked = true;
						_lockWait = transition.waitFor;
					}

					_upcomingState = newState;
					
					StartCoroutine( TransitionStepper( transition.transition ) );
				}
			}
			else
			{
                Debug.Log( "No transitions for current state: " + newState + " on object: " + gameObject );
				currentState = newState;
			}
		}
		else if ( !_states.Contains( newState ) )
			Debug.Log( "Given state does not exist: " + newState + " on object: " + gameObject );
		else if ( _locked )
			Debug.Log( "State locked" );
		else
			Debug.Log("Already in state " + newState);
	}

	public string GetState()
	{
		return currentState;
	}

#endregion

#region State Transitioning

	IEnumerator TransitionStepper( IEnumeratorFactory factory )
	{	
		IEnumerator transition = factory();
		
		if ( _changeAfter == ChangeState.first )
		{
			currentState = _upcomingState;
		}
				
		while ( transition.MoveNext() )
		{
			yield return transition.Current;
		}
		
		if ( _changeAfter == ChangeState.afterBoth )
			_changeAfter = ChangeState.afterEither;
		else if ( _changeAfter == ChangeState.afterEither )
		{
			_upcomingState = null;
			currentState = _upcomingState;
			_changeAfter = ChangeState.first;
		}
		
		if ( _lockWait == LockWait.forBoth )
		{
			_lockWait = LockWait.forEither;
		}
		else if ( _lockWait != LockWait.forNeither )
		{
			_locked = false;
			_lockWait = LockWait.forNeither;
		}
	}

#endregion

#region Cached components
	
	public Rigidbody rigidbody
	{
		get
		{
			if ( !_rigidbody )
				_rigidbody = gameObject.GetComponent< Rigidbody >();

            return _rigidbody;
		}
	}
	
	public Rigidbody2D rigidbody2D
	{
		get
		{
			if ( !_rigidbody2d )
                _rigidbody2d = gameObject.GetComponent<Rigidbody2D>();
				
            return _rigidbody2d;
		}
	}
	
	public TrailRenderer trailRenderer
	{
		get
		{
			if ( !_trailRenderer )
                _trailRenderer = gameObject.GetComponent<TrailRenderer>();
				
            return _trailRenderer;
		}
	}
	
	public LineRenderer lineRenderer
	{
		get
		{
			if ( !_lineRenderer )
                _lineRenderer = gameObject.GetComponent<LineRenderer>();

			return _lineRenderer;
		}
		
	}
	
	public SphereCollider sphereCollider
	{
		get
		{
            if ( !_sphereCollider )
                _sphereCollider = gameObject.GetComponent<SphereCollider>();

            return _sphereCollider;
		}
		
	}
	
	public Renderer renderer
	{
		get
		{
            if ( !_renderer )
                _renderer = gameObject.GetComponent<Renderer>();

            return _renderer;
		}
	}
	
	public Material material
	{
		get
		{
			if ( !_material )
				_material = renderer.material;
			
			return _material;
		}
		
		set
		{
			renderer.material = value;
		}
	}
	
	public BoxCollider boxCollider
	{
		get
		{
			if ( !_boxCollider )
				_boxCollider = gameObject.GetComponent< BoxCollider >() ;
			
			return _boxCollider;
		}
	}
	
	public CircleCollider2D circleCollider2D
	{
		get
		{
			if ( !_circleCollider2d )
				_circleCollider2d = gameObject.GetComponent< CircleCollider2D >();
				
            return _circleCollider2d;
		}
	}
	
	public BoxCollider2D boxCollider2D
	{
		get
		{
			if ( !_boxCollider2d )
				_boxCollider2d = gameObject.GetComponent< BoxCollider2D >();
			
			return _boxCollider2d;
		}
	}
	
	private GameObject _gameObject;
	private Transform _transform;
	private Transform _parent;
	private Rigidbody _rigidbody;
	private Rigidbody2D _rigidbody2d;
	private TrailRenderer _trailRenderer;
	private LineRenderer _lineRenderer;
	private SphereCollider _sphereCollider;
	private BoxCollider _boxCollider;
	private CircleCollider2D _circleCollider2d;
	private BoxCollider2D _boxCollider2d;
	private Renderer _renderer;
	private Material _material;
		
#endregion

#region Variables

    public enum LockWait
    {
        forNeither = 0 ,
        forEither = 1 ,
        forBoth = 2
    }

    public enum ChangeState
    {
        first = 0 ,
        afterEither = 1 ,
        afterBoth = 2
    }

    private LockWait _lockWait = LockWait.forNeither;

    public delegate IEnumerator IEnumeratorFactory();

    [SerializeField] protected string[] states;
    [SerializeField] protected string initialState;
    [SerializeField] protected string currentState;

    protected HashSet< string > _states = new HashSet< string >();
    protected Dictionary< StatePair , CustomTransition > customTransition = new Dictionary<StatePair , CustomTransition>();
    protected Dictionary< string , DefaultTransition > defaultOnEnter = new Dictionary<string , DefaultTransition>();
    protected Dictionary< string , DefaultTransition > defaultOnExit = new Dictionary<string , DefaultTransition>();

    private ChangeState _changeAfter;
    private string _upcomingState { get; set; }
    private bool _locked = false;

#endregion

#region Transition Structs

    public struct StatePair
    {
        public string exitingState;
        public string enteringState;

        public StatePair( string exitingState , string enteringState )
        {
            this.exitingState = exitingState;
            this.enteringState = enteringState;
        }
    }

    public struct CustomTransition
    {
        public IEnumeratorFactory exitTransition;
        public IEnumeratorFactory enterTransition;
        public LockWait waitFor;
        public bool locked;
        public ChangeState changeState;

        public CustomTransition( IEnumeratorFactory exitTransition , IEnumeratorFactory enterTransition , bool locked , LockWait waitFor , ChangeState changeState )
        {
            this.exitTransition = exitTransition;
            this.enterTransition = enterTransition;
            this.locked = locked;
            this.waitFor = waitFor;
            this.changeState = changeState;
        }

        public CustomTransition( IEnumeratorFactory exitTransition , IEnumeratorFactory enterTransition )
        {
            this.exitTransition = exitTransition;
            this.enterTransition = enterTransition;
            this.locked = false;
            this.waitFor = LockWait.forNeither;
            this.changeState = ChangeState.first;
        }
    }

    public struct DefaultTransition
    {
        public IEnumeratorFactory transition;
        public LockWait waitFor;
        public bool locked;
        public ChangeState changeState;

        public DefaultTransition( IEnumeratorFactory transition , bool locked , LockWait waitFor , ChangeState changeState )
        {
            this.transition = transition;
            this.locked = locked;
            this.waitFor = waitFor;
            this.changeState = changeState;
        }

        public DefaultTransition( IEnumeratorFactory transition )
        {
            this.transition = transition;
            this.locked = false;
            this.waitFor = LockWait.forNeither;
            this.changeState = ChangeState.first;
        }

    }

#endregion
}

