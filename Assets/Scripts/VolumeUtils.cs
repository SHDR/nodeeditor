﻿using UnityEngine;
using System.Collections;

public class VolumeUtils
{
    #region Volume management

    public static void GenerateVolume( VolumeDefinition volumeDef )
    {
        while ( volumeDef.layers < volumeDef.mapDimensions.z )
        {
            AddLayer( volumeDef );
            volumeDef.layers++;
        }

        while ( volumeDef.columns < volumeDef.mapDimensions.x )
        {
            AddColumn( volumeDef );
            volumeDef.columns++;
        }

        while ( volumeDef.rows < volumeDef.mapDimensions.y )
        {
            AddRow( volumeDef );
            volumeDef.rows++;
        }
    }

    public static void AddLayer( VolumeDefinition volumeDef )
    {
        for ( int y = 0 ; y < volumeDef.mapDimensions.y ; y++ )
        {
            for ( int x = 0 ; x < volumeDef.mapDimensions.x ; x++ )
            {
                int index = ( ( int ) volumeDef.maxMapDimensions.x * y ) + ( ( int ) ( volumeDef.maxMapDimensions.x * volumeDef.maxMapDimensions.y ) * volumeDef.layers ) + x;

                volumeDef.volumeMap[ index ] = 1;
            }
        }

        volumeDef.columns = ( int ) volumeDef.mapDimensions.x;
        volumeDef.rows = ( int ) volumeDef.mapDimensions.y;
    }

    public static void RemoveLayer( VolumeDefinition volumeDef )
    {
        for ( int y = 0 ; y < volumeDef.mapDimensions.y ; y++ )
        {
            for ( int x = 0 ; x < volumeDef.mapDimensions.x ; x++ )
            {
                int index = ( ( int ) volumeDef.maxMapDimensions.x * y ) + ( ( int ) ( volumeDef.maxMapDimensions.x * volumeDef.maxMapDimensions.y ) * volumeDef.layers ) + x;

                volumeDef.volumeMap[ index ] = 0;
            }
        }
    }

    public static void AddColumn( VolumeDefinition volumeDef )
    {
        for ( int z = 0 ; z < volumeDef.mapDimensions.z ; z++ )
        {
            for ( int y = 0 ; y < volumeDef.mapDimensions.y ; y++ )
            {
                int index = ( ( int ) volumeDef.maxMapDimensions.x * y ) + ( ( int ) ( volumeDef.maxMapDimensions.x * volumeDef.maxMapDimensions.y ) * z ) + volumeDef.columns;

                volumeDef.volumeMap[ index ] = 1;
            }
        }
    }

    public static void RemoveColumn( VolumeDefinition volumeDef )
    {
        for ( int z = 0 ; z < volumeDef.mapDimensions.z ; z++ )
        {
            for ( int y = 0 ; y < volumeDef.mapDimensions.y ; y++ )
            {
                int index = ( ( int ) volumeDef.maxMapDimensions.x * y ) + ( ( int ) ( volumeDef.maxMapDimensions.x * volumeDef.maxMapDimensions.y ) * z ) + volumeDef.columns;

                volumeDef.volumeMap[ index ] = 0;
            }
        }
    }

    public static void AddRow( VolumeDefinition volumeDef )
    {
        for ( int z = 0 ; z < volumeDef.mapDimensions.z ; z++ )
        {
            for ( int x = 0 ; x < volumeDef.mapDimensions.x ; x++ )
            {
                int index = ( ( int ) volumeDef.maxMapDimensions.x * volumeDef.rows ) + ( ( int ) ( volumeDef.maxMapDimensions.x * volumeDef.maxMapDimensions.y ) * z ) + x;

                volumeDef.volumeMap[ index ] = 1;
            }
        }
    }

    public static void RemoveRow( VolumeDefinition volumeDef )
    {
        for ( int z = 0 ; z < volumeDef.mapDimensions.z ; z++ )
        {
            for ( int x = 0 ; x < volumeDef.mapDimensions.x ; x++ )
            {
                int index = ( ( int ) volumeDef.maxMapDimensions.x * volumeDef.rows ) + ( ( int ) ( volumeDef.maxMapDimensions.x * volumeDef.maxMapDimensions.y ) * z ) + x;

                volumeDef.volumeMap[ index ] = 0;
            }
        }
    }

    #endregion

    #region Volume access

    public static void SetBlock( VolumeDefinition volumeDef , byte block )
    {
        Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
        RaycastHit hit;

        if ( Physics.Raycast( ray , out hit ) && hit.transform == volumeDef.nodeSystem.transform )
        {
            Vector3 position = hit.point;
            position -= volumeDef.nodeSystem.transform.position;

            if ( block == 0 || block == 255 )
                position -= ( hit.normal * 0.5f ) * volumeDef.tileScale;
            else
                position += ( hit.normal * 0.5f ) * volumeDef.tileScale;

            position.x += ( volumeDef.maxMapDimensions.x * 0.25f ) * volumeDef.tileScale;
            position.y += ( volumeDef.maxMapDimensions.x * 0.25f ) * volumeDef.tileScale;
            position.z += ( volumeDef.maxMapDimensions.x * 0.25f ) * volumeDef.tileScale;

            position = position / volumeDef.tileScale;

            int x = ( int ) Mathf.Round( position.x );
            int y = ( int ) Mathf.Round( position.y );
            int z = ( int ) Mathf.Round( position.z );

            if ( CellWithinBounds( x , y , z , volumeDef ) && GetCellAt( x , y , z , volumeDef ) != 255 )
            {
                SetCellAt( ( int ) Mathf.Round( position.x ) , ( int ) Mathf.Round( position.y ) , ( int ) Mathf.Round( position.z ) , block , volumeDef );
            }
            else if ( volumeDef.nodeSystem.parent != null )
            {
                position = hit.point;
                position -= volumeDef.nodeSystem.parent.transform.position;

                if ( block == 0 || block == 255 )
                    position -= ( hit.normal * 0.5f ) * volumeDef.nodeSystem.parent.volumeDef.tileScale;
                else
                    position += ( hit.normal * 0.5f ) * volumeDef.nodeSystem.parent.volumeDef.tileScale;

                position.x += ( volumeDef.nodeSystem.parent.volumeDef.maxMapDimensions.x * 0.25f ) * volumeDef.nodeSystem.parent.volumeDef.tileScale;
                position.y += ( volumeDef.nodeSystem.parent.volumeDef.maxMapDimensions.x * 0.25f ) * volumeDef.nodeSystem.parent.volumeDef.tileScale;
                position.z += ( volumeDef.nodeSystem.parent.volumeDef.maxMapDimensions.x * 0.25f ) * volumeDef.nodeSystem.parent.volumeDef.tileScale;

                position = position / volumeDef.nodeSystem.parent.volumeDef.tileScale;

                x = ( int ) Mathf.Round( position.x );
                y = ( int ) Mathf.Round( position.y );
                z = ( int ) Mathf.Round( position.z );

                if ( CellWithinBounds( x , y , z , volumeDef.nodeSystem.parent.volumeDef ) && GetCellAt( x , y , z , volumeDef.nodeSystem.parent.volumeDef ) != 255 )
                {
                    SetCellAt( ( int ) Mathf.Round( position.x ) , ( int ) Mathf.Round( position.y ) , ( int ) Mathf.Round( position.z ) , block , volumeDef.nodeSystem.parent.volumeDef );
                }
            }
            else if ( volumeDef.nodeSystem.children.Count > 0 )
            {
                foreach ( NodeSystem nodeSystem in volumeDef.nodeSystem.children.Values )
                {
                    position = hit.point;
                    position -= nodeSystem.transform.position;

                    if ( block == 0 || block == 255 )
                        position -= ( hit.normal * 0.5f ) * nodeSystem.volumeDef.tileScale;
                    else
                        position += ( hit.normal * 0.5f ) * nodeSystem.volumeDef.tileScale;

                    position.x += ( nodeSystem.volumeDef.maxMapDimensions.x * 0.25f ) * nodeSystem.volumeDef.tileScale;
                    position.y += ( nodeSystem.volumeDef.maxMapDimensions.x * 0.25f ) * nodeSystem.volumeDef.tileScale;
                    position.z += ( nodeSystem.volumeDef.maxMapDimensions.x * 0.25f ) * nodeSystem.volumeDef.tileScale;

                    position = position / nodeSystem.volumeDef.tileScale;

                    x = ( int ) Mathf.Round( position.x );
                    y = ( int ) Mathf.Round( position.y );
                    z = ( int ) Mathf.Round( position.z );

                    if ( CellWithinBounds( x , y , z , nodeSystem.volumeDef ) && GetCellAt( x , y , z , nodeSystem.volumeDef ) != 255 )
                    {
                        SetCellAt( ( int ) Mathf.Round( position.x ) , ( int ) Mathf.Round( position.y ) , ( int ) Mathf.Round( position.z ) , block , nodeSystem.volumeDef );
                    }
                }
            }
        }
    }

    public static bool CellWithinBounds ( int x , int y , int z , VolumeDefinition volumeDef )
    {
        if ( x >= volumeDef.maxMapDimensions.x || x < 0 || y >= volumeDef.maxMapDimensions.y || y < 0 || z >= volumeDef.maxMapDimensions.z || z < 0 )
            return false;
        else
            return true;
    }

    public static byte GetCellAt( int x , int y , int z , VolumeDefinition volumeDef )
    {
        if ( x >= volumeDef.maxMapDimensions.x || x < 0 || y >= volumeDef.maxMapDimensions.y || y < 0 || z >= volumeDef.maxMapDimensions.z || z < 0 )
            return 0;
        else
            return volumeDef.volumeMap[ ( ( int ) volumeDef.maxMapDimensions.x * y ) + ( ( int ) ( volumeDef.maxMapDimensions.x * volumeDef.maxMapDimensions.y ) * z ) + x ];
    }

    public static byte GetCellAt( int index , VolumeDefinition volumeDef )
    {
        if ( index + 1 > volumeDef.volumeMap.Length )
            return 0;
        else
            return volumeDef.volumeMap[ index ];
    }

    public static void SetCellAt( int x , int y , int z , byte tile , VolumeDefinition volumeDef )
    {
        if ( x + 1 > volumeDef.mapDimensions.x && x < volumeDef.maxMapDimensions.x )
        {
            volumeDef.mapDimensions = new Vector3( x + 1 , volumeDef.mapDimensions.y , volumeDef.mapDimensions.z );
            volumeDef.columns = x + 1;
        }

        if ( y + 1 > volumeDef.mapDimensions.y && y < volumeDef.maxMapDimensions.y )
        {
            volumeDef.mapDimensions = new Vector3( volumeDef.mapDimensions.x , y + 1 , volumeDef.mapDimensions.z );
            volumeDef.rows = y + 1;
        }

        if ( z + 1 > volumeDef.mapDimensions.z && y < volumeDef.maxMapDimensions.z )
        {
            volumeDef.mapDimensions = new Vector3( volumeDef.maxMapDimensions.x , volumeDef.maxMapDimensions.y , z + 1 );
            volumeDef.layers = z + 1;
        }

        if ( x < volumeDef.maxMapDimensions.x && x >= 0 && y < volumeDef.maxMapDimensions.y && y >= 0 && z < volumeDef.maxMapDimensions.z && z >= 0 )
        {
            volumeDef.volumeMap[ ( int ) ( ( ( volumeDef.maxMapDimensions.x * y ) + ( ( volumeDef.maxMapDimensions.x * volumeDef.maxMapDimensions.y ) * z ) + x ) ) ] = tile;
            volumeDef.nodeSystem.UpdateMesh();
        }
    }

    public static void SetCellAt( int index , byte tile , VolumeDefinition volumeDef )
    {
        if ( index + 1 < volumeDef.volumeMap.Length )
        {
            volumeDef.volumeMap[ index ] = tile;

            volumeDef.nodeSystem.UpdateMesh();
        }
    }

    #endregion
}
