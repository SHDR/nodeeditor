﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MeshUtils
{
    #region Mesh generation

    public static void GenerateMesh( MeshDefinition meshDef , VolumeDefinition volumeDef )
    {
        meshDef.triangles = new int[ ( int ) ( volumeDef.mapDimensions.x * volumeDef.mapDimensions.y * volumeDef.mapDimensions.z ) * 36 ];
        meshDef.uv = new Vector2[ ( int ) ( volumeDef.mapDimensions.x * volumeDef.mapDimensions.y * volumeDef.mapDimensions.z ) * 24 ];
        meshDef.vertices = new Vector3[ ( int ) ( volumeDef.mapDimensions.x * volumeDef.mapDimensions.y * volumeDef.mapDimensions.z ) * 24 ];

        for ( int z = 0 ; z < volumeDef.mapDimensions.z ; z++ )
        {
            for ( int y = 0 ; y < volumeDef.mapDimensions.y ; y++ )
            {
                for ( int x = 0 ; x < volumeDef.mapDimensions.x ; x++ )
                {
                    int index = ( ( int ) volumeDef.maxMapDimensions.x * y ) + ( ( int ) ( volumeDef.maxMapDimensions.x * volumeDef.maxMapDimensions.y ) * z ) + x;

                    if ( volumeDef.volumeMap[ index ] > 0 && volumeDef.volumeMap[ index ] < 255 )
                    {
                        if ( VolumeUtils.GetCellAt( x , y + 1 , z , volumeDef ) == 0 || VolumeUtils.GetCellAt( x , y + 1 , z , volumeDef ) == 255 )
                            GenerateFace( x , y , z , 0 , meshDef , volumeDef , MeshFace.top );

                        if ( VolumeUtils.GetCellAt( x , y - 1 , z , volumeDef ) == 0 || VolumeUtils.GetCellAt( x , y - 1 , z , volumeDef ) == 255 )
                            GenerateFace( x , y , z , 0 , meshDef , volumeDef , MeshFace.bottom );

                        if ( VolumeUtils.GetCellAt( x , y , z + 1 , volumeDef ) == 0 || VolumeUtils.GetCellAt( x , y , z + 1 , volumeDef ) == 255 )
                            GenerateFace( x , y , z , 0 , meshDef , volumeDef , MeshFace.north );

                        if ( VolumeUtils.GetCellAt( x , y , z - 1 , volumeDef ) == 0 || VolumeUtils.GetCellAt( x , y , z - 1 , volumeDef ) == 255 )
                            GenerateFace( x , y , z , 0 , meshDef , volumeDef , MeshFace.south );

                        if ( VolumeUtils.GetCellAt( x + 1 , y , z , volumeDef ) == 0 || VolumeUtils.GetCellAt( x + 1 , y , z , volumeDef ) == 255 )
                            GenerateFace( x , y , z , 0 , meshDef , volumeDef , MeshFace.east );

                        if ( VolumeUtils.GetCellAt( x - 1 , y , z , volumeDef ) == 0 || VolumeUtils.GetCellAt( x - 1 , y , z , volumeDef ) == 255 )
                            GenerateFace( x , y , z , 0 , meshDef , volumeDef , MeshFace.west );
                    }
                    else if ( volumeDef.volumeMap[ index ] == 255 && !volumeDef.nodeSystem.children.ContainsKey( index ) )
                    {
                        GameObject nodeSystemGO = new GameObject();

                        NodeSystem newNodeSystem = nodeSystemGO.AddComponent<NodeSystem>();

                        newNodeSystem.tileScale = volumeDef.tileScale * 0.5f;
                        newNodeSystem.material = meshDef.material;

                        Vector3 newPos = volumeDef.nodeSystem.transform.position + new Vector3( x * volumeDef.tileScale , y * volumeDef.tileScale , z * volumeDef.tileScale );

                        newPos.x -= ( volumeDef.maxMapDimensions.x * 0.25f * volumeDef.tileScale );
                        newPos.y -= ( volumeDef.maxMapDimensions.y * 0.25f * volumeDef.tileScale );
                        newPos.z -= ( volumeDef.maxMapDimensions.z * 0.25f * volumeDef.tileScale );

                        nodeSystemGO.transform.position = newPos;
                        nodeSystemGO.transform.name = "NodeSystem";

                        newNodeSystem.maxMapDimensions = new Vector3( 2 , 2 , 2 );
                        newNodeSystem.mapDimensions = new Vector3( 2 , 2 , 2 );

                        volumeDef.nodeSystem.AddChild( index , newNodeSystem );
                    }
                }
            }
        }
    }

    public static void MakeTriangles( Vector2 texturePos , MeshDefinition meshDef , VolumeDefinition volumeDef )
    {
        meshDef.triangles[ 0 + ( 6 * meshDef.faceCount ) ] = meshDef.faceCount * 4;
        meshDef.triangles[ 1 + ( 6 * meshDef.faceCount ) ] = meshDef.faceCount * 4 + 1;
        meshDef.triangles[ 2 + ( 6 * meshDef.faceCount ) ] = meshDef.faceCount * 4 + 2;
        meshDef.triangles[ 3 + ( 6 * meshDef.faceCount ) ] = meshDef.faceCount * 4;
        meshDef.triangles[ 4 + ( 6 * meshDef.faceCount ) ] = meshDef.faceCount * 4 + 2;
        meshDef.triangles[ 5 + ( 6 * meshDef.faceCount ) ] = meshDef.faceCount * 4 + 3;

        meshDef.uv[ 0 + ( 4 * meshDef.faceCount ) ] = new Vector2( meshDef.tUnit * texturePos.x + meshDef.tUnit , meshDef.tUnit * texturePos.y );
        meshDef.uv[ 1 + ( 4 * meshDef.faceCount ) ] = new Vector2( meshDef.tUnit * texturePos.x + meshDef.tUnit , meshDef.tUnit * texturePos.y + meshDef.tUnit );
        meshDef.uv[ 2 + ( 4 * meshDef.faceCount ) ] = new Vector2( meshDef.tUnit * texturePos.x , meshDef.tUnit * texturePos.y + meshDef.tUnit );
        meshDef.uv[ 3 + ( 4 * meshDef.faceCount ) ] = new Vector2( meshDef.tUnit * texturePos.x , meshDef.tUnit * texturePos.y );

        meshDef.faceCount++;
    }

    public static void GenerateFace( int x , int y , int z , byte block , MeshDefinition meshDef , VolumeDefinition volumeDef , MeshFace direction )
    {
        Vector2 texturePos = Vector2.up;

        switch ( direction )
        {
            case MeshFace.top:

                if ( VolumeUtils.GetCellAt( x , y + 1 , z , volumeDef ) == 0 )
                    texturePos = Vector2.zero;

                meshDef.vertices[ 0 + ( 4 * meshDef.faceCount ) ] = new Vector3( x - 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y + 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z + 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;
                meshDef.vertices[ 1 + ( 4 * meshDef.faceCount ) ] = new Vector3( x + 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y + 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z + 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;
                meshDef.vertices[ 2 + ( 4 * meshDef.faceCount ) ] = new Vector3( x + 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y + 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z - 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;
                meshDef.vertices[ 3 + ( 4 * meshDef.faceCount ) ] = new Vector3( x - 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y + 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z - 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;

                break;

            case MeshFace.bottom :

                meshDef.vertices[ 0 + ( 4 * meshDef.faceCount ) ] = new Vector3( x - 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y - 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z - 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;
                meshDef.vertices[ 1 + ( 4 * meshDef.faceCount ) ] = new Vector3( x + 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y - 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z - 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;
                meshDef.vertices[ 2 + ( 4 * meshDef.faceCount ) ] = new Vector3( x + 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y - 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z + 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;
                meshDef.vertices[ 3 + ( 4 * meshDef.faceCount ) ] = new Vector3( x - 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y - 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z + 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;

            break;

            case MeshFace.north :

                if ( VolumeUtils.GetCellAt( x , y + 1 , z , volumeDef ) == 0 )
                    texturePos = Vector2.up + Vector2.right;

                meshDef.vertices[ 0 + ( 4 * meshDef.faceCount ) ] = new Vector3( x + 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y - 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z + 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;
                meshDef.vertices[ 1 + ( 4 * meshDef.faceCount ) ] = new Vector3( x + 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y + 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z + 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;
                meshDef.vertices[ 2 + ( 4 * meshDef.faceCount ) ] = new Vector3( x - 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y + 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z + 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;
                meshDef.vertices[ 3 + ( 4 * meshDef.faceCount ) ] = new Vector3( x - 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y - 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z + 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;

                break;

            case MeshFace.south :
                

                if ( VolumeUtils.GetCellAt( x , y + 1 , z , volumeDef ) == 0 )
                    texturePos = Vector2.up + Vector2.right;

                meshDef.vertices[ 0 + ( 4 * meshDef.faceCount ) ] = new Vector3( x - 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y - 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z - 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;
                meshDef.vertices[ 1 + ( 4 * meshDef.faceCount ) ] = new Vector3( x - 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y + 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z - 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;
                meshDef.vertices[ 2 + ( 4 * meshDef.faceCount ) ] = new Vector3( x + 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y + 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z - 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;
                meshDef.vertices[ 3 + ( 4 * meshDef.faceCount ) ] = new Vector3( x + 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y - 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z - 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;

                break;

            case MeshFace.east :

                if ( VolumeUtils.GetCellAt( x , y + 1 , z , volumeDef ) == 0 )
                    texturePos = Vector2.up + Vector2.right;

                meshDef.vertices[ 0 + ( 4 * meshDef.faceCount ) ] = new Vector3( x + 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y - 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z - 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;
                meshDef.vertices[ 1 + ( 4 * meshDef.faceCount ) ] = new Vector3( x + 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y + 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z - 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;
                meshDef.vertices[ 2 + ( 4 * meshDef.faceCount ) ] = new Vector3( x + 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y + 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z + 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;
                meshDef.vertices[ 3 + ( 4 * meshDef.faceCount ) ] = new Vector3( x + 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y - 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z + 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;

                break;

            case MeshFace.west :

                if ( VolumeUtils.GetCellAt( x , y + 1 , z , volumeDef ) == 0 )
                    texturePos = Vector2.up + Vector2.right;

                meshDef.vertices[ 0 + ( 4 * meshDef.faceCount ) ] = new Vector3( x - 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y - 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z + 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;
                meshDef.vertices[ 1 + ( 4 * meshDef.faceCount ) ] = new Vector3( x - 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y + 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z + 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;
                meshDef.vertices[ 2 + ( 4 * meshDef.faceCount ) ] = new Vector3( x - 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y + 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z - 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;
                meshDef.vertices[ 3 + ( 4 * meshDef.faceCount ) ] = new Vector3( x - 0.5f - ( volumeDef.maxMapDimensions.x * 0.25f ) , y - 0.5f - ( volumeDef.maxMapDimensions.y * 0.25f ) , z - 0.5f - ( volumeDef.maxMapDimensions.z * 0.25f ) ) * volumeDef.tileScale;

                break;
        }

        MakeTriangles( texturePos , meshDef , volumeDef );
    }

    public enum MeshFace
    {
        top ,
        bottom ,
        north ,
        south ,
        east ,
        west
    }

    #endregion
}
