﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NodeSystem : MonoBehaviour
{
    #region Monobehaviour

    void Start ()
    {
        _volumeDef = new VolumeDefinition( mapDimensions , maxMapDimensions , tileScale , this );
        _meshDef = new MeshDefinition( material , 0.5f );

        VolumeUtils.GenerateVolume( _volumeDef );
        UpdateMesh();
    }

    void Update ()
    {
        if ( Input.GetMouseButtonUp( 0 ) )
        {
            VolumeUtils.SetBlock( _volumeDef , 1 );
        }

        if ( Input.GetMouseButtonUp( 1 ) )
        {
            VolumeUtils.SetBlock( _volumeDef , 0 );
        }

        if ( Input.GetKeyUp( KeyCode.Space ) )
        {
            VolumeUtils.SetBlock( _volumeDef , 255 );
        }

        int blockSum = 0;

        for ( int i = 0 ; i < _volumeDef.volumeMap.Length ; i++ )
        {
            blockSum += ( int ) _volumeDef.volumeMap[ i ];
        }

        if ( blockSum == 0 )
            parent.RemoveChild( siblingIndex );
    }

    #endregion

    #region Mesh generation

    public void UpdateMesh ()
    {
        MeshUtils.GenerateMesh( _meshDef , _volumeDef );

        _mesh.Clear();
        _mesh.vertices = _meshDef.vertices;
        _mesh.uv = _meshDef.uv;
        _mesh.triangles = _meshDef.triangles;
        _mesh.Optimize();
        _mesh.RecalculateNormals();

        _meshCollider.sharedMesh = null;
        _meshCollider.sharedMesh = _mesh;

        _meshDef.faceCount = 0;
    }

    #endregion

    #region Graph management

    public void AddChild( int index , NodeSystem nodeSystem )
    {
        nodeSystem.SetParent( this );
        nodeSystem.transform.parent = transform;
        nodeSystem.SetSiblingIndex( index );
        _children.Add( index , nodeSystem );
    }

    public void RemoveChild( int index )
    {
        if ( _children.ContainsKey( index ) )
        {
            VolumeUtils.SetCellAt( index , 0 , _volumeDef );

            Destroy( _children[ index ].gameObject );
            
            _children.Remove( index );
        }
    }

    public void SetSiblingIndex( int index )
    {
        _siblingIndex = index;
    }

    public void SetParent ( NodeSystem parent )
    {
        _parent = parent;
    }
    #endregion

    #region Variables

    #region Graph

    private int _siblingIndex;
    public int siblingIndex
    {
        get
        {
            return _siblingIndex;
        }
    }

    private Dictionary< int , NodeSystem > _children = new Dictionary< int , NodeSystem >();
    public Dictionary< int , NodeSystem > children
    {
        get
        {
            return _children;
        }
    }

    private NodeSystem _parent;
    public NodeSystem parent
    {
        get
        {
            return _parent;
        }
    }

    #endregion

    #region Volume

    public Vector3 maxMapDimensions;
    public Vector3 mapDimensions;
    public float tileScale = 1f;

    private VolumeDefinition _volumeDef;
    public VolumeDefinition volumeDef
    {
        get
        {
            return _volumeDef;
        }
    }

    #endregion

    #region Mesh

    public Material material;

    private Vector2 grassTop = new Vector2( 0 , 0 );
    private Vector2 dirtGrass = new Vector2( 1 , 1 );
    private Vector2 dirt = new Vector2( 0 , 1 );
    private Vector2 rock = new Vector2( 1 , 0 );

    private Mesh c_mesh;
    private Mesh _mesh
    {
        get
        {
            if ( c_mesh == null )
            {
                gameObject.AddComponent< MeshRenderer >().material = material;
                c_mesh = gameObject.AddComponent< MeshFilter >().mesh;
            }

            return c_mesh;
        }
    }


    private MeshCollider c_meshCollider;
    private MeshCollider _meshCollider
    {
        get
        {
            if ( c_meshCollider == null )
            {
                c_meshCollider = gameObject.AddComponent< MeshCollider >();
            }

            return c_meshCollider;
        }
    }

    private MeshDefinition _meshDef;
    public MeshDefinition meshDef
    {
        get
        {
            return _meshDef;
        }
    }
    #endregion
    
    #endregion
}

public class VolumeDefinition
{
    public Vector3 mapDimensions;
    public Vector3 maxMapDimensions;
    public byte[] volumeMap;
    public int rows;
    public int columns;
    public int layers;
    public float tileScale;
    public NodeSystem nodeSystem;

    public VolumeDefinition( Vector3 mapDimensions , Vector3 maxMapDimensions , float tileScale , NodeSystem nodeSystem )
    {
        this.mapDimensions = mapDimensions;
        this.maxMapDimensions = maxMapDimensions;
        this.volumeMap = new byte[ ( int ) ( this.maxMapDimensions.x * this.maxMapDimensions.y * this.maxMapDimensions.z ) ];
        this.tileScale = tileScale;
        this.rows = 0;
        this.columns = 0;
        this.layers = 0;
        this.nodeSystem = nodeSystem;
    }
}

public class MeshDefinition
{
    public int[] triangles;
    public Vector2[] uv;
    public Vector3[] vertices;
    public Material material;
    public float tUnit;
    public int faceCount;

    public MeshDefinition( Material material , float tUnit )
    {
        this.material = material;
        this.tUnit = tUnit;
        this.faceCount = 0;
        this.triangles = null;
        this.uv = null;
        this.vertices = null;
    }
}
/*
NOTE MAKING TIME
 * 
 *  First of all, what's the purpose of the recursive thing?
 *      Make assets
 *      Better building/destruction
 *      
 *  OK, why do we need to improve the recursive thing?
 *      Better sooner rather than later, it must be done sometime anyway
 *      Compatibility -- if the tool is to be used, the functionality must be fixed
 *      
 *  How can we fix the recursive thing?
 *      Need to declare big chunks rather than small chunks
 *      Right now, a single cell is replaced, and that's that. Hm.
 *      Maybe that's why this isn't done so often. :P
 *      
 *  What's the problem?
 *      The children will need to be literally overlaid the parent
 *      There will be sorting/collision issues
 *      There will be issues of knowing whether a space is occupied or not -- or is that same as above ..?
 *      But they are solvable
 *      
 *  What needs to be done?
 *      Subdivide the grid -- will need to be 4 new grids the same dimensions as parent, but at 1/4 scale
 *      They need to be positioned accurately
 *      Then the cell you subdivided needs to be filled in -- all four of them, even
 *      Sounds hard, man
 *      
 *  Let's try anyway


































*/